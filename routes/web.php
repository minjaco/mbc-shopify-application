<?php
//Logs route
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')
    ->middleware('basicAuth');

//Envoyer beat route
Route::get('/beat', static function () {
    return ['status' => 'success'];
});

//Designer identifier key
Route::post('/save-key', 'Apps\SaveKeyController')
    ->middleware(['auth.shop'])
    ->name('save-key');

//shopify main route
Route::get('/', 'Apps\ShopifyController')
    ->middleware(['auth.shop'])
    ->name('home');
