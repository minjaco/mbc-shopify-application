<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('designer_id')->unsigned()->index();
            $table->uuid('identifier')->nullable();
            $table->string('name');
            $table->string('email');
            $table->string('customer_email');
            $table->string('domain');
            $table->string('province');
            $table->string('city');
            $table->string('country_code');
            $table->string('country_name');
            $table->string('shop_owner');
            $table->string('myshopify_domain');
            $table->string('enabled_presentment_currencies');//implode with comma
            $table->point('location');
            $table->tinyInteger('has_gift_cards');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_information');
    }
}
