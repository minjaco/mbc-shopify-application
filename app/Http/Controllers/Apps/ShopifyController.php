<?php

namespace App\Http\Controllers\Apps;

use App\Models\ShopifyShopInformation;
use App\Http\Controllers\Controller;

class ShopifyController extends Controller
{

    public function __invoke()
    {
        $shop    = \ShopifyApp::shop();
        $request = $shop->api()->rest('GET', '/admin/shop.json');

        $product_data = $shop->api()->rest('GET', '/admin/products.json',
        ['limit' => 1, 'published_status' => 'published', 'since_id' => 1]);

        dd($request->body->shop, $product_data->response->getHeaders(), $product_data->body);

        //$options = collect($products->first()->options)->where('name', 'Color');

        //dd($request->body->shop, $products, $options);

        $info = app(ShopifyShopInformation::class)
            ->with('designer')
            ->where('shop_id', $request->body->shop->id)
            ->first();

        if (!$info) {
            return ['message' => 'Setup Application Again'];
        }

        if (!$info->designer) {
            return view('apps.shopify-enter-key');
        }

        $info->load('syncData');

        $designer              = $info->designer;
        $synced_products_count = $info->syncData->last()->total_products ?? '-';
        $last_sync             = isset($info->syncData->last()->synced_at) ? $info->syncData->last()->synced_at->format('F d, Y') : '-';

        return view('apps.shopify', compact('designer', 'synced_products_count', 'last_sync'));
    }
}
