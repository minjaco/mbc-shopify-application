<?php

namespace App\Http\Controllers\Apps;

use App\Http\Controllers\Controller;
use App\Http\Requests\KeyRequest;
use App\Models\Designer;
use App\Models\ShopifyShopInformation;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;

class SaveKeyController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  KeyRequest  $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function __invoke(KeyRequest $request)
    {

        try {
            $designer = app(Designer::class)
                ->where('identifier', $request->key)
                ->firstOrFail();

            $shop = \ShopifyApp::shop();
            $info = $shop->api()->rest('GET', '/admin/shop.json');

            $shop_info = app(ShopifyShopInformation::class)
                ->where('shop_id', $info->body->shop->id)
                ->first();

            $shop_info->designer_id = $designer->id;
            $shop_info->save();

            return redirect()->route('home');

        } catch (ModelNotFoundException $exception) {
            return redirect()->route('home')->with('error', true);
        }
    }
}
