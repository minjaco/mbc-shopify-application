<?php

namespace App\Console\Commands;

use App\Models\ShopifyCurrencyRate;
use Illuminate\Console\Command;

class UpdateRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:currencies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates currencies';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $data = file_get_contents('https://api.exchangeratesapi.io/latest?base=USD');
        $json = json_decode($data);

        $symbols = json_decode(\Storage::disk('local')->get('currency.json'), true);

        app(ShopifyCurrencyRate::class)->query()->truncate();

        foreach ($json->rates as $key => $val) {

            $symbol_data = $symbols[$key];

            app(ShopifyCurrencyRate::class)
                ->create([
                    'currency' => $key,
                    'symbol'   => $symbol_data['symbol_native'],
                    'rate'     => $val,
                ]);
        }

        $this->info('Done!');
    }
}
