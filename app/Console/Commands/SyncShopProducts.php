<?php

namespace App\Console\Commands;

use App\Jobs\SyncProducts;
use Illuminate\Console\Command;
use OhMyBrew\ShopifyApp\Models\Shop;

class SyncShopProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Syncs shop products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $shops = app(Shop::class)->all();

        foreach ($shops as $shop) {
            dispatch(new SyncProducts($shop));
        }

        $this->info('Done!');
    }
}
