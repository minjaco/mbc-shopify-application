<?php

namespace App\Jobs;

use App\Models\ShopifyShopInformation;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use OhMyBrew\ShopifyApp\Models\Shop;

class AfterAuthenticateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's instance.
     */
    protected $shop;

    /**
     * Create a new job instance.
     *
     * @param  Shop  $shop  The shop's object
     *
     */
    public function __construct(Shop $shop)
    {
        $this->shop = $shop;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $request = $this->shop->api()->rest('GET', '/admin/shop.json');
        $s       = $request->body->shop;

        app(ShopifyShopInformation::class)
            ->create([
                'shop_id'                        => $s->id,
                'name'                           => $s->name,
                'email'                          => $s->email,
                'customer_email'                 => $s->customer_email,
                'domain'                         => $s->domain,
                'province'                       => $s->province,
                'city'                           => $s->city,
                'country_code'                   => $s->country_code,
                'country_name'                   => $s->country_name,
                'shop_owner'                     => $s->shop_owner,
                'myshopify_domain'               => $s->myshopify_domain,
                'enabled_presentment_currencies' => implode(',', $s->enabled_presentment_currencies),
                'location'                       => new Point($s->latitude, $s->longitude),
                'has_gift_cards'                 => $s->has_gift_cards ? 1 : 0,
            ]);

    }
}
