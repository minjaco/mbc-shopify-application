<?php

namespace App\Jobs;

use App\Mail\SendSyncErrorMail;
use App\Models\DesignerProduct;
use App\Models\ShopifyCurrencyRate;
use App\Models\ShopifyShopInformation;
use App\Models\ShopifySyncDatum;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use OhMyBrew\ShopifyApp\Models\Shop;

class SyncProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $shop;
    public $limit;

    /**
     * Create a new job instance.
     *
     * @param  Shop  $shop
     * @param  int  $limit
     */
    public function __construct(Shop $shop, $limit = 250)
    {
        $this->shop  = $shop;
        $this->limit = $limit;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $count_data = $this->shop->api()->rest(
            'GET',
            '/admin/products/count.json',
            ['published_status' => 'published']
        );

        $count = $count_data->body->count;

        //note: Get shop id from header
        $shopify_shop_id = $count_data->response->getHeader('X-ShopId')[0];

        //note: Get shop information from DB
        $shop_information = app(ShopifyShopInformation::class)->where('shop_id', $shopify_shop_id)->first();

        //region Currency
        $currency = explode(',', $shop_information->enabled_presentment_currencies)[0];

        //note: Determine divide factor
        $factor = 1;

        if ($currency !== 'USD') {
            $currency_information = app(ShopifyCurrencyRate::class)->where('currency', $currency)->first();
            //Set divide factor
            $factor = $currency_information->rate;
        }
        //endregion currency

        //note: Determine page count
        $page = ceil($count / $this->limit);

        $since = 1;

        for ($i = 1; $i <= $page; $i++) {

            $product_data = $this->shop->api()->rest(
                'GET',
                '/admin/products.json',
                ['limit' => $this->limit, 'published_status' => 'published', 'since_id' => $since]
            );

            $products = collect($product_data->body->products);

            foreach ($products as $product) {

                $price_in_usd = 0.00;

                $check_product = app(DesignerProduct::class)
                    ->where('shop_id', $shopify_shop_id)
                    ->where('product_id', $product->id)
                    ->first();

                //note: Check product if exist
                if (!$check_product) {

                    /*foreach ($product->variants as $variant) {
                        $price_in_usd = money_format('%i', ceil($variant->price / $factor));
                    }*/

                    if ($product->variants) {
                        //todo : price etc.
                        $price_in_usd = money_format('%i', ceil($product->variants->first()->price / $factor));
                    }

                    foreach ($product->images as $image) {
                        dispatch(new ProcessProductImage($image, $product->id, $shopify_shop_id));
                    }

                    $options = collect($product->options)->where('name', 'Color');

                    foreach ($options as $option) {
                        //todo : colors etc.
                    }

                    //note: Create new product
                    app(DesignerProduct::class)->create([
                        'shop_id'           => $shopify_shop_id,
                        'product_id'        => $product->id,
                        'product_type'      => $product->product_type,
                        'name'              => $product->title,
                        'description'       => $product->body_html,
                        'price'             => $price_in_usd,
                        'target_url'        => "https://$this->shop->domain/products/$product->handle",
                        'status'            => 1,
                        'remote_updated_at' => Carbon::parse($product->updated_at)->format('Y-m-d H:i:s'),
                    ]);

                } elseif ($check_product->remote_updated_at->lt(Carbon::parse($product->updated_at))) {

                    /*foreach ($product->variants as $variant) {
                        $price_in_usd = money_format('%i', ceil($variant->price / $factor));
                    }*/

                    if ($product->variants) {
                        //todo : price etc.
                        $price_in_usd = money_format('%i', ceil($product->variants->first()->price / $factor));
                    }

                    foreach ($product->images as $image) {
                        dispatch(new ProcessProductImage($image, $product->id, $shopify_shop_id));
                    }

                    $options = collect($product->options)->where('name', 'Color');

                    foreach ($options as $option) {
                        //todo : colors etc.
                    }


                    //note: Update product
                    $check_product->update([]);
                }
            }

            //note: Get last product
            $last_product = $products->last();
            $since        = $last_product->id;
        }

        //note: Create sync log
        app(ShopifySyncDatum::class)->create([
            'shop_id'        => $shopify_shop_id,
            'total_products' => $count,
            'synced_at'      => Carbon::now(),
        ]);
    }

    public function failed()
    {
        $message = (new SendSyncErrorMail($this->shop))
            ->onQueue('emails');

        \Mail::to(env('ADMIN_EMAIL'))
            ->queue($message);
    }
}
