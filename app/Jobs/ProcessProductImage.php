<?php

namespace App\Jobs;

use App\Models\DesignerProductImage;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Intervention\Image\Facades\Image;

class ProcessProductImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $image;
    public $product_id;
    public $shop_id;

    /**
     * Create a new job instance.
     *
     * @param object $image
     * @param int $product_id
     * @param int $shop_id
     */
    public function __construct($image, $product_id, $shop_id)
    {
        $this->image      = $image;
        $this->product_id = $product_id;
        $this->shop_id    = $shop_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $check_image = app(DesignerProductImage::class)
            ->where('shop_id', $this->shop_id)
            ->where('product_id', $this->product_id)
            ->where('image_id', $this->image->id)
            ->first();

        if (!$check_image) {

            [$new_url, $new_url_thumb] = $this->handleCloudOperation();

            app(DesignerProductImage::class)
                ->create([
                    'image_id'          => $this->image->id,
                    'shop_id'           => $this->shop_id,
                    'product_id'        => $this->product_id,
                    'url'               => $new_url,
                    'thumb_url'         => $new_url_thumb,
                    'remote_updated_at' => Carbon::parse($this->image->updated_at),
                ]);
        }

        if ($check_image->remote_updated_at->lt(Carbon::parse($this->image->updated_at))) {

            [$new_url, $new_url_thumb] = $this->handleCloudOperation();

            $check_image->update([
                'url'               => $new_url,
                'thumb_url'         => $new_url_thumb,
                'remote_updated_at' => Carbon::parse($this->image->updated_at),
            ]);

        }

    }

    /**
     * @return array
     */
    private function handleCloudOperation(): array
    {
        $shop = $this->shop_id;

        $name = strtok(basename($this->image->src), '?');

        $path = $this->image->src;

        $img_thumb = Image::make($path)->fit(480)->encode('jpg', 80);

        $img = Image::make($path)->resize(1280, null, static function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg', 80);

        $resource       = $img->stream()->detach();
        $resource_thumb = $img_thumb->stream()->detach();

        //Upload to digital ocean space
        \Storage::cloud()
            ->put("mbc/shopify/$shop/$name", $resource, 'public');

        \Storage::cloud()
            ->put("mbc/shopify/$shop/thumb-$name", $resource_thumb, 'public');

        $new_url       = env('DO_SPACES_CDN_ENDPOINT')."mbc/shopify/$shop/$name";
        $new_url_thumb = env('DO_SPACES_CDN_ENDPOINT')."mbc/shopify/$shop/thumb-$name";

        return [$new_url, $new_url_thumb];
    }


}
