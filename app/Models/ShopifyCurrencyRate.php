<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 11 Dec 2019 01:46:13 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ShopifyCurrencyRate
 *
 * @property int $id
 * @property string $currency
 * @property float $rate
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class ShopifyCurrencyRate extends Model
{
    protected $table = 'currency_rates';

    protected $casts = [
        'rate' => 'float',
    ];

    protected $fillable = [
        'currency',
        'symbol',
        'rate',
    ];
}
