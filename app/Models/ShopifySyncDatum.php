<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 06 Dec 2019 06:11:44 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ShopifySyncDatum
 *
 * @property int $id
 * @property int $shop_id
 * @property int $total_products
 * @property \Carbon\Carbon $synced_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class ShopifySyncDatum extends Model
{

    protected $table = 'sync_data';

    protected $casts = [
        'shop_id'        => 'int',
        'total_products' => 'int',
    ];

    protected $dates = [
        'synced_at',
    ];

    protected $fillable = [
        'shop_id',
        'total_products',
        'synced_at',
    ];
}
