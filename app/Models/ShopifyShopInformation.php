<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 06 Dec 2019 04:40:27 +0000.
 */

namespace App\Models;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ShopifyShopInformation
 *
 * @property int $id
 * @property string $identifier
 * @property string $name
 * @property string $email
 * @property string $customer_email
 * @property string $domain
 * @property string $province
 * @property string $city
 * @property string $country_code
 * @property string $country_name
 * @property string $shop_owner
 * @property string $myshopify_domain
 * @property string $enabled_presentment_currencies
 * @property point $location
 * @property int $has_gift_cards
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class ShopifyShopInformation extends Model
{

    use SpatialTrait;

    protected $table = 'shop_information';

    protected $casts = [
        'shop_id'        => 'int',
        'has_gift_cards' => 'int',
    ];

    protected $spatialFields = [
        'location',
    ];

    protected $fillable = [
        'shop_id',
        'identifier',
        'name',
        'email',
        'customer_email',
        'domain',
        'province',
        'city',
        'country_code',
        'country_name',
        'shop_owner',
        'myshopify_domain',
        'enabled_presentment_currencies',
        'location',
        'has_gift_cards',
    ];

    public function designer()
    {
        return $this->belongsTo(Designer::class,'identifier','identifier');
    }

    public function syncData()
    {
        return $this->hasMany(ShopifySyncDatum::class, 'shop_id', 'shop_id');
    }
}
