<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Oct 2019 10:15:53 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DesignerProduct
 *
 * @property int $id
 * @property int $designer_id
 * @property int $shop_id
 * @property int $product_id
 * @property string $name
 * @property string $product_type
 * @property string $description
 * @property string $price
 * @property string $target_url
 * @property int $status
 * @property int $type
 * @property \Carbon\Carbon $remote_updated_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class DesignerProduct extends Model
{

    protected $connection = 'mysql_no_prefix';

    protected $casts = [
        'designer_id' => 'int',
        'shop_id'     => 'int',
        'product_id'  => 'int',
        'status'      => 'int',
        'type'        => 'int',
    ];

    protected $dates = [
        'remote_updated_at',
    ];

    protected $fillable = [
        'designer_id',
        'shop_id',
        'product_id',
        'product_type',
        'name',
        'description',
        'price',
        'target_url',
        'status',
        'type',
        'remote_updated_at',
    ];
}
